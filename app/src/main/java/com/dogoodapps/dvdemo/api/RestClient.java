package com.dogoodapps.dvdemo.api;


import com.dogoodapps.dvdemo.api.request.Credentials;
import com.dogoodapps.dvdemo.entity.Session;
import com.dogoodapps.dvdemo.entity.User;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;
import rx.schedulers.Schedulers;

// https://zeroturnaround.com/rebellabs/getting-started-with-retrofit-2/
public final class RestClient {

	private static final String API_URL = "http://dogoodapps.eu-2.evennode.com/";

	private static final String API_TOKEN = "apiToken";

	private static DVDemoService dvDemoService;

	public static DVDemoService getService() {
		if (dvDemoService == null) {
			final Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(API_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
				.build();

			dvDemoService = retrofit.create(DVDemoService.class);
		}
		return dvDemoService;
	}

	public interface DVDemoService {

		@POST("/session/new")
		@Headers("Content-Type: application/json")
		Observable<Session> createNewSession(@Body Credentials credentials);

		@GET("/users/{userId}")
		@Headers("Content-Type: application/json")
		Observable<User> getUser(@Header(API_TOKEN) String apiKey, @Path("userId") String userId);

	}

}