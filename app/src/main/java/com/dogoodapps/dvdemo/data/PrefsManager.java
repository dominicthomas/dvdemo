package com.dogoodapps.dvdemo.data;

import android.content.SharedPreferences;
import android.net.Uri;

import com.securepreferences.SecurePreferences;

// TODO: Tests
// See: https://github.com/scottyab/secure-preferences
public class PrefsManager {

	private static final String KEY_EMAIL = "KEY_EMAIL";

	private static final String KEY_PASSWORD = "KEY_PASSWORD";

	private static final String KEY_API_TOKEN = "KEY_API_TOKEN";

	private static final String KEY_USER_ID = "KEY_USER_ID";

	private static final String KEY_PROFILE_PIC = "KEY_PROFILE_PIC";

	private final SharedPreferences sharedPreferences;

	public PrefsManager(SecurePreferences securePreferences) {
		this.sharedPreferences = securePreferences;
	}

	public void clear() {
		sharedPreferences.edit().clear().apply();
	}

	// TODO: User Generics!
	public void saveEmail(String email) {
		sharedPreferences.edit().putString(KEY_PASSWORD, email).apply();
	}

	public String getEmail() {
		return sharedPreferences.getString(KEY_EMAIL, null);
	}

	public void savePassword(String password) {
		sharedPreferences.edit().putString(KEY_PASSWORD, password).apply();
	}

	public String getPassword() {
		return sharedPreferences.getString(KEY_PASSWORD, null);
	}

	public void saveApiKey(final String apiToken) {
		sharedPreferences.edit().putString(KEY_API_TOKEN, apiToken).apply();
	}

	public String getApiToken() {
		return sharedPreferences.getString(KEY_API_TOKEN, null);
	}

	public void saveUserId(String userId) {
		sharedPreferences.edit().putString(KEY_USER_ID, userId).apply();
	}

	public String getUserId() {
		return sharedPreferences.getString(KEY_USER_ID, null);
	}

	public void saveImageUri(Uri uri) {
		sharedPreferences.edit().putString(KEY_PROFILE_PIC, uri.toString()).apply();
	}

	public Uri getImageUri() {
		return Uri.parse(sharedPreferences.getString(KEY_PROFILE_PIC, null));
	}
}
