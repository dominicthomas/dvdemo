package com.dogoodapps.dvdemo.entity;

public class Session {

	private final String userId;

	private final String token;

	public Session(String userId, String token) {
		this.userId = userId;
		this.token = token;
	}

	public String getUserId() {
		return userId;
	}

	public String getToken() {
		return token;
	}
}
