package com.dogoodapps.dvdemo.entity;

public class User {

	private final String email;

	private final String avatarUrl;

	public User(String email, String avatarUrl) {
		this.email = email;
		this.avatarUrl = avatarUrl;
	}

	public String getEmail() {
		return email;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

}
