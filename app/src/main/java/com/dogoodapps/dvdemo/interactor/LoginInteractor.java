package com.dogoodapps.dvdemo.interactor;

import com.dogoodapps.dvdemo.api.RestClient;
import com.dogoodapps.dvdemo.api.request.Credentials;
import com.dogoodapps.dvdemo.data.PrefsManager;
import com.dogoodapps.dvdemo.entity.Session;
import com.dogoodapps.dvdemo.router.RouteFactory;

import rx.Observable;

public class LoginInteractor {

	private final RouteFactory routeFactory;

	private final PrefsManager prefsManager;

	public LoginInteractor(RouteFactory routeFactory, PrefsManager prefsManager) {
		this.routeFactory = routeFactory;
		this.prefsManager = prefsManager;
	}

	public Observable<Session> createNewSession(String email, String password) {
		return RestClient.getService().createNewSession(new Credentials(email, password));
	}

	public void onSessionCreated(Session session) {
		prefsManager.saveApiKey(session.getToken());
		prefsManager.saveUserId(session.getUserId());
		routeFactory.launchProfileActivity();
	}

	public void savePassword(String password) {
		prefsManager.savePassword(password);
	}

	public void saveEmail(String email) {
		prefsManager.saveEmail(email);
	}
}
