package com.dogoodapps.dvdemo.interactor;

import android.content.Intent;
import android.net.Uri;

import com.dogoodapps.dvdemo.api.RestClient;
import com.dogoodapps.dvdemo.data.PrefsManager;
import com.dogoodapps.dvdemo.entity.User;
import com.dogoodapps.dvdemo.router.RouteFactory;

import rx.Observable;

public class ProfileInteractor {

	// TODO: Save image location in prefs

	// TODO: Routefactory can link us to camera?


	private final RouteFactory routeFactory;

	private final PrefsManager prefsManager;

	public ProfileInteractor(RouteFactory routeFactory, PrefsManager prefsManager) {
		this.routeFactory = routeFactory;
		this.prefsManager = prefsManager;
	}

	public Observable<User> getUser(String apiToken, String userId) {
		return RestClient.getService().getUser(apiToken, userId);
	}

	public String getPassword() {
		return prefsManager.getPassword();
	}

	public Uri getImageUri() {
		return prefsManager.getImageUri();
	}

	public String getApiToken() {
		return prefsManager.getApiToken();
	}

	public void logOut() {
		prefsManager.clear();
		routeFactory.launchMainActivity();
	}

	public Intent getCameraIntent() {
		return routeFactory.getCameraIntent();
	}

	public Intent getGalleryIntent() {
		return routeFactory.getGalleryIntent();
	}

	public void saveImageUri(Uri uri) {
		prefsManager.saveImageUri(uri);
	}

}
