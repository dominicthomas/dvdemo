package com.dogoodapps.dvdemo.presenter;

import android.support.annotation.NonNull;

import rx.Subscription;

public abstract class BasePresenter<T> {

	private final T view;

	public BasePresenter(T view) {
		this.view = view;
	}

	public T getView() {
		return view;
	}

	protected void unsubscribe(@NonNull Subscription subscription){
		if(!subscription.isUnsubscribed()){
			subscription.unsubscribe();
		}
	}
}
