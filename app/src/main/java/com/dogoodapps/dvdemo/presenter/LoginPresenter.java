package com.dogoodapps.dvdemo.presenter;

import android.text.TextUtils;

import com.dogoodapps.dvdemo.entity.Session;
import com.dogoodapps.dvdemo.interactor.LoginInteractor;
import com.dogoodapps.dvdemo.view.LoginView;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class LoginPresenter extends BasePresenter<LoginView> implements Observer<Session> {

	private final LoginInteractor loginInteractor;

	private Subscription subscription;

	public LoginPresenter(LoginView view, LoginInteractor loginInteractor) {
		super(view);
		this.loginInteractor = loginInteractor;
	}

	public void onLoginClicked(String email, String password) {
		if (TextUtils.isEmpty(email)) {
			getView().showMessage("Email address missing!");
			return;
		}

		if (TextUtils.isEmpty(password)) {
			getView().showMessage("Password cannot be black!");
			return;
		}

		loginInteractor.saveEmail(email);
		loginInteractor.savePassword(password);
		subscription = loginInteractor
			.createNewSession(email, password)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(this);
	}

	@Override
	public void onCompleted() {
		unsubscribe(subscription);
	}

	@Override
	public void onError(Throwable e) {
		getView().showMessage(e.getMessage());
	}

	@Override
	public void onNext(Session session) {
		loginInteractor.onSessionCreated(session);
		getView().loginComplete();
	}

}
