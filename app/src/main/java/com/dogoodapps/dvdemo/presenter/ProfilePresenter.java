package com.dogoodapps.dvdemo.presenter;

import android.content.Intent;
import android.net.Uri;
import android.view.MenuItem;

import com.dogoodapps.dvdemo.R;
import com.dogoodapps.dvdemo.entity.User;
import com.dogoodapps.dvdemo.interactor.ProfileInteractor;
import com.dogoodapps.dvdemo.view.ProfileView;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

public class ProfilePresenter extends BasePresenter<ProfileView> implements Observer<User> {

	private final ProfileInteractor profileInteractor;

	private Subscription subscription;

	public ProfilePresenter(ProfileView view, ProfileInteractor profileInteractor) {
		super(view);
		this.profileInteractor = profileInteractor;
	}

	public void loadUser(String apiToken, String userId) {
		subscription = profileInteractor
			.getUser(apiToken, userId)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(this);
	}

	@Override
	public void onCompleted() {
		unsubscribe(subscription);
	}

	@Override
	public void onError(Throwable e) {
		getView().showMessage(e.getMessage());
	}

	@Override
	public void onNext(User user) {
		//getView().displayProfilePic(Uri.parse(user.getAvatarUrl()));
		getView().showEmail(user.getEmail());
		getView().showPassword(profileInteractor.getPassword());
		getView().showMessage(profileInteractor.getApiToken());
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_logout:
				profileInteractor.logOut();
				getView().logoutComplete();
				break;
			default:
				break;
		}
		return true;
	}

	public void onProfilePicClicked(int requestProfilePhoto) {
		getView().showImagePickerDialog();
	}

	public Intent getCameraIntent() {
		return profileInteractor.getCameraIntent();
	}

	public Intent getGalleryIntent() {
		return profileInteractor.getGalleryIntent();
	}

	public void onImageLoaded(Uri uri) {
		profileInteractor.saveImageUri(uri);
	}
}
