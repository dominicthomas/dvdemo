package com.dogoodapps.dvdemo.router;

import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;

import com.dogoodapps.dvdemo.ui.activity.LoginActivity;
import com.dogoodapps.dvdemo.ui.activity.MainActivity;
import com.dogoodapps.dvdemo.ui.activity.ProfileActivity;

public class RouteFactory {

	private final Context context;

	public RouteFactory(Context context) { // TODO: Review context
		this.context = context;
	}

	public void launchMainActivity() {
		final Intent intent = new Intent(context, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}

	public void launchLoginActivity() {
		final Intent intent = new Intent(context, LoginActivity.class);
		context.startActivity(intent);
	}

	public void launchProfileActivity() {
		final Intent intent = new Intent(context, ProfileActivity.class);
		context.startActivity(intent);
	}

	public Intent getCameraIntent() {
		return new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	}

	public Intent getGalleryIntent() {
		return new Intent(Intent.ACTION_PICK,
			android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	}

}
