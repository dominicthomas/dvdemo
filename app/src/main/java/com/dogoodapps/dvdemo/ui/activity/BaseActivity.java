package com.dogoodapps.dvdemo.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutId());
		ButterKnife.bind(this);
		initialise();
	}

	protected abstract int getLayoutId();

	@Override
	protected void onStart() {
		super.onStart();
	}

	protected void initialise() {
		// empty
	}

}
