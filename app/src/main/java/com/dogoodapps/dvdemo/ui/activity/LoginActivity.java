package com.dogoodapps.dvdemo.ui.activity;

import android.widget.EditText;
import android.widget.Toast;

import com.dogoodapps.dvdemo.R;
import com.dogoodapps.dvdemo.data.PrefsManager;
import com.dogoodapps.dvdemo.interactor.LoginInteractor;
import com.dogoodapps.dvdemo.presenter.LoginPresenter;
import com.dogoodapps.dvdemo.router.RouteFactory;
import com.dogoodapps.dvdemo.view.LoginView;
import com.securepreferences.SecurePreferences;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginView {

	@BindView(R.id.editTextEmail)
	EditText emailAddress;

	@BindView(R.id.editTextPassword)
	EditText password;

	private LoginPresenter loginPresenter;

	@Override
	protected void initialise() {
		final RouteFactory routeFactory = new RouteFactory(this);
		final SecurePreferences securePreferences = new SecurePreferences(this);
		final PrefsManager prefsManager = new PrefsManager(securePreferences);
		final LoginInteractor loginInteractor = new LoginInteractor(routeFactory, prefsManager);
		loginPresenter = new LoginPresenter(this, loginInteractor);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.activity_login;
	}

	@Override
	@OnClick(R.id.buttonLogin)
	public void onLoginClicked() {
		loginPresenter.onLoginClicked(
			emailAddress.getText().toString(),
			password.getText().toString());
	}

	@Override
	public void showMessage(String message) {
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void loginComplete() {
		finish();
	}

}
