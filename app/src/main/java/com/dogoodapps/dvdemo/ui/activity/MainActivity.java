package com.dogoodapps.dvdemo.ui.activity;

import android.text.TextUtils;

import com.dogoodapps.dvdemo.R;
import com.dogoodapps.dvdemo.data.PrefsManager;
import com.dogoodapps.dvdemo.router.RouteFactory;
import com.securepreferences.SecurePreferences;

public class MainActivity extends BaseActivity {

	@Override
	protected void initialise() {
		final SecurePreferences securePreferences = new SecurePreferences(this);
		final PrefsManager prefsManager = new PrefsManager(securePreferences);
		final RouteFactory routeFactory = new RouteFactory(this);
		if (!TextUtils.isEmpty(prefsManager.getApiToken())) {
			routeFactory.launchProfileActivity();
		} else {
			routeFactory.launchLoginActivity();
		}
		finish();
	}

	@Override
	protected int getLayoutId() {
		return R.layout.activity_main;
	}

}
