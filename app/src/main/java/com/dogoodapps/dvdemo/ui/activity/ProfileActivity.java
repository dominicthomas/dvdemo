package com.dogoodapps.dvdemo.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dogoodapps.dvdemo.R;
import com.dogoodapps.dvdemo.data.PrefsManager;
import com.dogoodapps.dvdemo.interactor.ProfileInteractor;
import com.dogoodapps.dvdemo.presenter.ProfilePresenter;
import com.dogoodapps.dvdemo.router.RouteFactory;
import com.dogoodapps.dvdemo.view.ProfileView;
import com.securepreferences.SecurePreferences;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileActivity extends BaseActivity implements ProfileView {

	private static final int REQUEST_PROFILE_PHOTO = 100;

	@BindView(R.id.imageProfilePic)
	ImageView profilePic;

	@BindView(R.id.textEmailAddress)
	TextView emailAddressTextView;

	@BindView(R.id.textPassword)
	TextView passwordTextView;

	private ProfilePresenter profilePresenter;

	@Override
	protected void initialise() {
		final RouteFactory routeFactory = new RouteFactory(this);
		final SecurePreferences securePreferences = new SecurePreferences(this);
		final PrefsManager prefsManager = new PrefsManager(securePreferences);
		final ProfileInteractor profileInteractor = new ProfileInteractor(routeFactory, prefsManager);
		profilePresenter = new ProfilePresenter(this, profileInteractor);
		profilePresenter.loadUser(prefsManager.getApiToken(), prefsManager.getUserId());
		profilePic.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.activity_profile;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return profilePresenter.onOptionsItemSelected(item);
	}

	@Override
	public void displayProfilePic(Uri uri) {
		if (uri != null) {
			profilePic.setImageBitmap(null);
			profilePic.setImageURI(uri);
			profilePic.setRotation(-90);
			profilePresenter.onImageLoaded(uri);
		}
	}

	@Override
	public void showEmail(String email) {
		emailAddressTextView.setText(email);
	}

	@Override
	public void showPassword(String password) {
		passwordTextView.setText(password);
	}


	@OnClick(R.id.imageProfilePic)
	public void onProfilePicClicked() {
		profilePresenter.onProfilePicClicked(REQUEST_PROFILE_PHOTO);
	}

	@Override
	public void showImagePickerDialog() { // TODO: Move to presenter!
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Profile Picture");
		alertDialog.setMessage("Choose image source");
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Camera",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					showCamera();
					dialog.dismiss();
				}
			});
		alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Gallery",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					showGallery();
					dialog.dismiss();
				}
			});
		alertDialog.show();
	}

	@Override
	public void showCamera() {
		startActivityForResult(
			profilePresenter.getCameraIntent(),
			REQUEST_PROFILE_PHOTO);
	}

	@Override
	public void showGallery() {
		startActivityForResult(
			profilePresenter.getGalleryIntent(),
			REQUEST_PROFILE_PHOTO);
	}

	@Override
	public void showMessage(String message) {
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
		switch (requestCode) {
			case REQUEST_PROFILE_PHOTO:
				if (resultCode == RESULT_OK) {
					// TODO: Downsize, Rotate etc..
					displayProfilePic(imageReturnedIntent.getData());
				}
				break;
		}
	}

	@Override
	public void logoutComplete() {
		finish();
	}

}
