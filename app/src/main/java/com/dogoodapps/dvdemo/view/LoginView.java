package com.dogoodapps.dvdemo.view;

public interface LoginView {

	void onLoginClicked();

	void showMessage(String message);

	void loginComplete();
}
