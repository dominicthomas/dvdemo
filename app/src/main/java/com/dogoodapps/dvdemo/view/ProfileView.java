package com.dogoodapps.dvdemo.view;

import android.net.Uri;

public interface ProfileView {

	void displayProfilePic(Uri uri);

	void showEmail(String email);

	void showPassword(String password);

	void logoutComplete();

	void showImagePickerDialog();

	void showCamera();

	void showGallery();

	void showMessage(String message);
}
